# Malaria Detection - Logistic Regression Classifier

•	The dataset contains two set of images, ‘Parasitized’ and ‘Uninfected’, each with 13779 cell images. The ‘Parasitized’ set contains the images that are malaria infected, and ‘Uninfected’ set contains the images that are NOT malaria infected.

•	This project has two python notebook named ‘Extracting_Image_Features_into_Tabular_Data.ipynb’ and ‘main_file.ipynb’. The first notebook deals with converting the image data into a tabular form, which contains the areas of the 5 distinct contours formed on the image. The second notebook deals with the model fitting to create a machine learning based classifier.

•	We convert the categorical (object) type column ‘diagnosis’ to numerical (int) type.

•	In the preprocessing step, we normalize our data to make it more well-conditioned to modelling. It is also important to note that fitting classifiers to un-normalized data often lead to longer convergence time.

•	The heatmaps are used to investigate the collinearity among the predictor variables, using the seaborn python library. There didn't seem to be a problem of strong collinearity among the predictor variables.

•	In the building models process, the data is divided into train and test splits with 80-20% proportions.

•	8 different machine learning regression models like SGDClassifier, RandomForestClassifier, LogisticsRegression, KNeighborsClassifier, Gaussian Naive Bayes, Perceptron, LinearSVC, DecisionTreeClassifier are fitted to the training data. Different models are compared based on the 3 different accuracy metrics (on training set - 80%, on test set - 20%, and mean5CV - on full data).

•	It is important to note here that when the data was not normalized, linearSVC was not converging even with high max_iter value, however with a normalized data, the problem becomes more well-conditioned for the algorithm, and it converged with default max_iter value.

•	It was found out that the Logistic Regression performs the best among all models we fit, across 'Score_mean5CV' and 'Score_test' accuracy scores.

•	It is interesting to note that Random Forest and Decision Tree models overfitted our data (leading to higher variance), because they have a highest (almost perfect) accuracy on training set but doesn't perform very well on the testing set. So, we chose Logistic Regression as our final model. Then, we tuned its hyperparameters.

•	The hyperparameters like ‘solver’, ‘penalty’, and ‘C’ are tuned using the GridSearchCV using Stratified 5-Fold cross validation to maintain the consistency of classes frequency in train and test splits. However, there wasn’t much change in the accuracy across all three metrics, after tuning.

•	The final model’s performance was evaluated based on confusion matrix, precision (89.6%), recall (90.51%) and F-score (90.06%).

•	ROC-AUC-Score means the area under the ROC curve. It represents the area under the blue curve. The red curve represents a completely random classifier (bad classifier). Now since our classifier’s ROC-AUC-Score was quite close to 1 (an excellent classifier), our model did a pretty good job at classifying the human cells into "Parasitized" or "Uninfected" classes.

***Techniques Used:*** SGDClassifier, RandomForestClassifier, LogisticsRegression, KNeighborsClassifier, Gaussian Naive Bayes, Perceptron, LinearSVC, DecisionTreeClassifier, GridSearchCV, RepeatedStratifiedKFold, seaborn, sklearn. (**Python**) 
